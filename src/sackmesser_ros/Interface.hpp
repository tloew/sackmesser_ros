/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ros/node_handle.h>

#include <sackmesser/Configuration.hpp>
#include <sackmesser/Interface.hpp>
#include <sackmesser/Logger.hpp>

namespace sackmesser_ros
{

    class PublisherFactory;
    class SubscriberFactory;

    class Interface : public sackmesser::Interface
    {
      public:
        Interface(ros::NodeHandle &node_handle, const std::shared_ptr<sackmesser::Logger> &logger = std::make_shared<sackmesser::Logger>());

        virtual ~Interface();

        ros::NodeHandle &getNodeHandle();

        bool ok() const;

      protected:
      private:
        struct Configuration : public sackmesser::Configuration
        {
            bool load(const std::string &ns, const std::shared_ptr<sackmesser::Configurations> &server);

            std::vector<std::string> publishers;

            std::vector<std::string> subscribers;
        };

        Configuration config_;

        ros::NodeHandle node_handle_;

        std::vector<std::unique_ptr<PublisherFactory>> publishers_;

        std::vector<std::unique_ptr<SubscriberFactory>> subscribers_;

      public:
        using Ptr = std::shared_ptr<Interface>;

      public:
        static sackmesser_ros::Interface::Ptr create(int argc, char **argv,
                                                     const std::shared_ptr<sackmesser::Logger> &logger = std::make_shared<sackmesser::Logger>());
    };

}  // namespace sackmesser_ros