/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <sackmesser/Callbacks.hxx>
#include <sackmesser_ros/Subscriber.hpp>

namespace sackmesser_ros
{

    template <class Message, class Type>
    Subscriber<Message, Type>::Subscriber(const std::string &name, Interface *interface) : interface_(interface)
    {
        std::string topic;

        if (interface->getConfigurations()->loadParameter("subscriber/" + name + "/topic", &topic))
        {
            subscriber_ = interface->getNodeHandle().subscribe(topic, 1, &Subscriber::callback, this);

            interface->log()->info() << "subscribing to topic " << topic << std::endl;
        }

        if (interface->getConfigurations()->loadParameter("subscriber/" + name + "/callback_queue", &callback_queue_))
        {
            interface->getCallbacks()->addQueue<Type>(callback_queue_);
        }
    }

    template <class Message, class Type>
    void Subscriber<Message, Type>::callback(const typename Message::ConstPtr &message)
    {
        interface_->getCallbacks()->invoke(callback_queue_, this->convert(message));
    }

    template <class Message, class Type>
    Interface *Subscriber<Message, Type>::getInterface()
    {
        return interface_;
    }

}  // namespace sackmesser_ros