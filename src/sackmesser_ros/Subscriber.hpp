/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <sackmesser/Configurations.hpp>
#include <sackmesser_ros/Interface.hpp>
//
#include <sackmesser/FactoryClass.hpp>

namespace sackmesser_ros
{
    class SubscriberFactory : public sackmesser::FactoryClass<SubscriberFactory, const std::string &, Interface *>
    {
      public:
        SubscriberFactory();

        virtual ~SubscriberFactory();
    };

    template <class Message, class Type>
    class Subscriber : public SubscriberFactory
    {
      public:
        Subscriber(const std::string &name, Interface *interface);

        Interface *getInterface();

      protected:
        virtual Type convert(const typename Message::ConstPtr &message) const = 0;

      private:
        void callback(const typename Message::ConstPtr &message);

        ros::Subscriber subscriber_;

        Interface *interface_;

        std::string callback_queue_;
    };
}  // namespace sackmesser_ros

#include <sackmesser_ros/Subscriber.hxx>
