/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ros/package.h>

#include <sackmesser/Callbacks.hpp>
#include <sackmesser_ros/Configurations.hpp>
#include <sackmesser_ros/Interface.hpp>
#include <sackmesser_ros/Publisher.hpp>
#include <sackmesser_ros/Subscriber.hpp>

namespace sackmesser_ros
{
    bool Interface::Configuration::load(const std::string & /*ns*/, const std::shared_ptr<sackmesser::Configurations> &server)
    {
        return server->loadParameter("publishers", &publishers) &&  //
               server->loadParameter("subscribers", &subscribers);
    }

    Interface::Interface(ros::NodeHandle &node_handle, const std::shared_ptr<sackmesser::Logger> &logger)
      : sackmesser::Interface(std::make_shared<Configurations>(node_handle, logger),  //
                              std::make_shared<sackmesser::Callbacks>(logger),        //
                              logger),
        node_handle_(node_handle)
    {
        config_ = getConfigurations()->load<Configuration>("");

        for (const std::string &publisher : config_.publishers)
        {
            std::string type;

            if (getConfigurations()->loadParameter("publisher/" + publisher + "/type", &type))
            {
                publishers_.push_back(PublisherFactory::getFactory()->createUnique(type, publisher, this));
            }
            else
            {
                this->log()->warn() << "no publisher " << publisher << " added" << std::endl;
            }
        }

        for (const std::string &subscriber : config_.subscribers)
        {
            std::string type;

            if (getConfigurations()->loadParameter("subscriber/" + subscriber + "/type", &type))
            {
                subscribers_.push_back(SubscriberFactory::getFactory()->createUnique(type, subscriber, this));
            }
            else
            {
                this->log()->warn() << "no subscriber " << subscriber << " added" << std::endl;
            }
        }
    }

    Interface::~Interface()
    {
        this->log()->info() << "Interface: closing" << std::endl;
    }

    bool Interface::ok() const
    {
        return ros::ok();
    }

    ros::NodeHandle &Interface::getNodeHandle()
    {
        return node_handle_;
    }

    sackmesser_ros::Interface::Ptr Interface::create(int argc, char **argv, const std::shared_ptr<sackmesser::Logger> &logger)
    {
        ros::init(argc, argv, "node");
        ros::NodeHandle node_handle("~");

        return std::make_shared<Interface>(node_handle, logger);
    }

}  // namespace sackmesser_ros