/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ros/package.h>

#include <sackmesser/Logger.hpp>
#include <sackmesser_ros/Configurations.hpp>

namespace sackmesser_ros
{
    Configurations::Configurations(ros::NodeHandle &node_handle, const std::shared_ptr<sackmesser::Logger> &logger)
      : sackmesser::Configurations(logger)
    {
        node_handle_ = node_handle;
        pub_description_ = node_handle.advertise<dynamic_reconfigure::ConfigDescription>("parameter_descriptions", 1, true);
        pub_value_ = node_handle.advertise<dynamic_reconfigure::Config>("parameter_updates", 1, true);
        srv_set_parameters_ = node_handle.advertiseService("set_parameters", &Configurations::updateParameters, this);
    }

    Configurations::~Configurations() {}

    bool Configurations::load(const std::string &name, bool *param)
    {
        return loadParam(name, *param, false);
    }

    bool Configurations::load(const std::string &name, double *param)
    {
        return loadParam(name, *param, 0.0);
    }

    bool Configurations::load(const std::string &name, int *param)
    {
        return loadParam(name, *param, 0);
    }

    bool Configurations::load(const std::string &name, unsigned *param)
    {
        int value;

        if (!loadParam(name, value, 0))
        {
            return false;
        }

        *param = static_cast<unsigned>(value);

        return true;
    }

    bool Configurations::load(const std::string &name, std::string *param)
    {
        return loadParam(name, *param, std::string(""));
    }

    bool Configurations::load(const std::string &name, std::map<std::string, double> *param)
    {
        return loadParam(name, *param, std::map<std::string, double>());
    }

    bool Configurations::load(const std::string &name, std::vector<std::string> *param)
    {
        return loadParam(name, *param, std::vector<std::string>());
    }

    bool Configurations::load(const std::string &name, std::vector<double> *param)
    {
        return loadParam(name, *param, std::vector<double>());
    }

    bool Configurations::load(const std::string &name, std::vector<int> *param)
    {
        return loadParam(name, *param, std::vector<int>());
    }

    bool Configurations::updateParameters(dynamic_reconfigure::Reconfigure::Request &req, dynamic_reconfigure::Reconfigure::Response &res)
    {
        bool success = true;

        for (const dynamic_reconfigure::BoolParameter &bool_parameter : req.config.bools)
        {
            success &= reconfigure<bool>(bool_parameter.name, bool_parameter.value);
        }

        for (const dynamic_reconfigure::IntParameter &int_parameter : req.config.ints)
        {
            success &= reconfigure<int>(int_parameter.name, int_parameter.value);
        }

        for (const dynamic_reconfigure::DoubleParameter &double_parameter : req.config.doubles)
        {
            success &= reconfigure<double>(double_parameter.name, double_parameter.value);
        }

        publish();

        res.config = config_description_.dflt;

        pub_value_.publish(config_description_.dflt);

        return success;
    }

    void Configurations::publish()
    {
        std::map<std::string, std::any> *dynamic_parameters = getDynamicParameters();
        const std::map<std::string, std::any> &parameters_min_val = getParametersMinimum();
        const std::map<std::string, std::any> &parameters_max_val = getParametersMaximum();

        dynamic_reconfigure::ConfigDescription config_description;

        int group_id = 1;
        std::map<int, std::vector<std::pair<std::string, std::string>>> parameters;
        std::map<std::string, std::pair<int, int>> groups;

        groups.emplace("sackmesser", std::make_pair(0, 0));

        for (std::pair<std::string, std::any> param : *dynamic_parameters)
        {
            std::vector<std::string> split;  // = sackmesser::split_string(param.first, '/');

            if (groups.find(split.front()) == groups.end())
            {
                groups.emplace(std::make_pair(split.front(), std::make_pair(group_id, 0)));

                group_id++;
            }

            for (unsigned i = 1; i < split.size() - 1; ++i)
            {
                if (groups.find(split[i]) == groups.end())
                {
                    groups.emplace(std::make_pair(split[i], std::make_pair(group_id, groups.at(split[i - 1]).first)));

                    group_id++;
                }
            }

            parameters[groups.at(*(split.rbegin() + 1)).first].push_back(std::make_pair(param.first, split.back()));
        }

        for (const std::pair<std::string, std::pair<int, int>> group : groups)
        {
            dynamic_reconfigure::Group group_description;

            group_description.name = group.first;
            group_description.id = group.second.first;
            group_description.parent = group.second.second;
            group_description.type = (group.second.second == 0) ? "tab" : "";

            dynamic_reconfigure::GroupState group_state;

            group_state.state = true;
            group_state.name = group.first;
            group_state.id = group.second.first;
            group_state.parent = group.second.second;

            if (parameters.find(group.second.first) != parameters.end())
            {
                for (const std::pair<std::string, std::string> &param : parameters.at(group.second.first))
                {
                    if (std::any_cast<bool *>(&dynamic_parameters->at(param.first)))
                    {
                        dynamic_reconfigure::ParamDescription param_description;

                        param_description.name = param.first;
                        param_description.type = "bool";
                        param_description.level = 0;
                        param_description.description = "";
                        param_description.edit_method = "";

                        group_description.parameters.push_back(param_description);

                        dynamic_reconfigure::BoolParameter bool_parameter;

                        bool_parameter.name = param.first;

                        bool_parameter.value = true;
                        config_description.max.bools.push_back(bool_parameter);

                        bool_parameter.value = false;
                        config_description.min.bools.push_back(bool_parameter);

                        bool_parameter.value = *std::any_cast<bool *>(dynamic_parameters->at(param.first));
                        config_description.dflt.bools.push_back(bool_parameter);
                    }
                    else if (std::any_cast<int *>(&dynamic_parameters->at(param.first)))
                    {
                        dynamic_reconfigure::ParamDescription param_description;

                        param_description.name = param.first;
                        param_description.type = "int";
                        param_description.level = 0;
                        param_description.description = "";
                        param_description.edit_method = "";

                        group_description.parameters.push_back(param_description);

                        dynamic_reconfigure::IntParameter int_parameter;

                        int_parameter.name = param.first;

                        int_parameter.value = std::any_cast<int>(parameters_max_val.at(param.first));
                        config_description.max.ints.push_back(int_parameter);

                        int_parameter.value = std::any_cast<int>(parameters_min_val.at(param.first));
                        config_description.min.ints.push_back(int_parameter);

                        int_parameter.value = *std::any_cast<int *>(dynamic_parameters->at(param.first));
                        config_description.dflt.ints.push_back(int_parameter);
                    }
                    else if (std::any_cast<double *>(&dynamic_parameters->at(param.first)))
                    {
                        dynamic_reconfigure::ParamDescription param_description;

                        param_description.name = param.first;
                        param_description.type = "double";
                        param_description.level = 0;
                        param_description.description = "";
                        param_description.edit_method = "";

                        group_description.parameters.push_back(param_description);

                        dynamic_reconfigure::DoubleParameter double_parameter;

                        double_parameter.name = param.first;

                        double_parameter.value = std::any_cast<double>(parameters_max_val.at(param.first));
                        config_description.max.doubles.push_back(double_parameter);

                        double_parameter.value = std::any_cast<double>(parameters_min_val.at(param.first));
                        config_description.min.doubles.push_back(double_parameter);

                        double_parameter.value = *std::any_cast<double *>(dynamic_parameters->at(param.first));
                        config_description.dflt.doubles.push_back(double_parameter);
                    }
                }
            }

            config_description.groups.push_back(group_description);
            config_description.max.groups.push_back(group_state);
            config_description.min.groups.push_back(group_state);
            config_description.dflt.groups.push_back(group_state);
        }

        config_description_ = config_description;

        pub_description_.publish(config_description_);
        pub_value_.publish(config_description_.dflt);
    }

}  // namespace sackmesser_ros