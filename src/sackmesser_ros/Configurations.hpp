/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <dynamic_reconfigure/Config.h>
#include <dynamic_reconfigure/ConfigDescription.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <ros/node_handle.h>

#include <sackmesser/Configurations.hpp>

namespace sackmesser_ros
{
    class Configurations : public sackmesser::Configurations
    {
      public:
        Configurations(ros::NodeHandle &node_handle, const std::shared_ptr<sackmesser::Logger> &logger);

        virtual ~Configurations();

        void publish();

      protected:
        bool updateParameters(dynamic_reconfigure::Reconfigure::Request &req, dynamic_reconfigure::Reconfigure::Response &rsp);

        bool load(const std::string &, bool *);

        bool load(const std::string &, double *);

        bool load(const std::string &, int *);

        bool load(const std::string &, unsigned *);

        bool load(const std::string &, std::string *);

        bool load(const std::string &, std::map<std::string, double> *);

        bool load(const std::string &, std::vector<std::string> *);

        bool load(const std::string &, std::vector<double> *);

        bool load(const std::string &, std::vector<int> *);

      private:
        template <class Param>
        bool loadParam(const std::string &name, Param &param, const Param &default_value)
        {
            if (!node_handle_.param(name, param, default_value))
            {
                this->log()->warn() << "Configurations: failed to find parameter " << name << std::endl;

                return false;
            }

            this->log()->info() << "Configurations: loaded " << name << std::endl;

            return true;
        }

      private:
        ros::NodeHandle node_handle_;

        ros::Publisher pub_description_;

        ros::Publisher pub_value_;

        ros::ServiceServer srv_set_parameters_;

        dynamic_reconfigure::ConfigDescription config_description_;
    };

}  // namespace sackmesser_ros