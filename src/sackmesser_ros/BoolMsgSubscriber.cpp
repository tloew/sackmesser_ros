/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sackmesser_ros/BoolMsgSubscriber.hpp>

namespace sackmesser_ros
{

    BoolMsgSubscriber::BoolMsgSubscriber(const std::string &name, Interface *interface)
      : sackmesser_ros::Subscriber<std_msgs::Bool, bool>(name, interface)
    {}

    bool BoolMsgSubscriber::convert(const typename std_msgs::Bool::ConstPtr &message) const
    {
        return message->data;
    }
}  // namespace sackmesser_ros

REGISTER_CLASS(sackmesser_ros::SubscriberFactory, sackmesser_ros::BoolMsgSubscriber, "std_msgs_bool");
