/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of sackmesser
 *
 * sackmesser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sackmesser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sackmesser.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <sackmesser/Callbacks.hpp>
#include <sackmesser_ros/Configurations.hpp>
#include <sackmesser_ros/Publisher.hpp>

namespace sackmesser_ros
{

    template <class MsgType, class... Arguments>
    Publisher<MsgType, Arguments...>::Publisher(const std::string &name, Interface *interface) : interface_(interface)
    {
        std::string topic, callback_queue;

        if (interface->getConfigurations()->loadParameter("publisher/" + name + "/topic", &topic))
        {
            publisher_ = interface->getNodeHandle().advertise<MsgType>(topic, 1, false);
        }

        if (interface->getConfigurations()->loadParameter("publisher/" + name + "/callback_queue", &callback_queue))
        {
            interface->getCallbacks()->addQueue<Arguments...>(callback_queue);
            interface->getCallbacks()->addCallbackToQueue<Arguments...>(callback_queue,
                                                                        [this](const Arguments &...arguments) { publish(arguments...); });
        }
    }

    template <class MsgType, class... Arguments>
    Publisher<MsgType, Arguments...>::~Publisher() = default;

    template <class MsgType, class... Arguments>
    void Publisher<MsgType, Arguments...>::publish(const Arguments &...arguments) const
    {
        MsgType message = createMessage(arguments...);

        publisher_.publish(message);
    }

}  // namespace sackmesser_ros